import { LitElement, html, css } from 'lit-element';

import '@material/mwc-icon-button';

class HelmholtzMarketplaceApp extends LitElement {
    constructor() {
        super();
        this.addEventListener('helmholtz-main-menu-toggle', this._toggleDrawer);
    }

    static get properties() {
        return {
            src: { type: String },
        };
    }

    static get styles()
    {
        return css`
            :host {
                display: block;
            }
            * {
                box-sizing: border-box;
            }
            .body {
                width: 100vw;
                height: 100vh;
                min-width: 320px;
                overflow-x: auto;
            }
            .body .main {
                height: 100%;
                width: 100%;
            }
            nav {
                width: 100%;
                height: 100%;
                position: absolute;
                transform: translate(0, -100%);
                transition: transform 0.7s ease;
                background-color: #fff;
                z-index: 10;
            }
            nav.open {
                transform: translate(0, 0);
            }
            nav .drawer-content {
                box-sizing: border-box;
                padding-left: 10%;
                padding-right: 10%;
            }
            nav .drawer-content a {
                min-height: 48px;
                width: 100%;
                display: flex;
                align-items: center;
                border-bottom: 1px solid #e2e2e2;
                text-decoration: none;
            }
            nav .drawer-content a:last-of-type {
                border-bottom: none;
            }
            .drawer-icon {
                display: flex;
                place-content: center;
                color: #005aa0;
                max-height: 65px;
                padding: 2% 0 2% 0;
                border-bottom: 1px solid rgb(226, 226, 226);
            }
            .drawer-icon img {
                height: 24px;
                width: fit-content;
                margin: auto;
            }
            @media screen and (min-width: 700px) {
                nav {
                    display: none;
                }
            }
        `;
    }

    render()
    {
        return html`
            <div class="body">
                <nav>
                    <div class="drawer-icon">
                        <mwc-icon-button
                            icon="close"
                            @click="${this._toggleDrawer}"></mwc-icon-button>
                        <img src="media/i/HIFIS_icon.svg"></img>
                    </div>
                    <div class="drawer-content">
                        <a target="_blank"
                            rel="noopener noreferrer"
                            href="https://hifis.net/team">Team</a>
                        <a target="_blank"
                            rel="noopener noreferrer"
                            href="https://hifis.net/news">News</a>
                        <a target="_blank"
                            rel="noopener noreferrer"
                            href="https://hifis.net/contact">Contact</a>
                        <a target="_blank"
                            rel="noopener noreferrer"
                            href="https://hifis.net/cloud-platform">About</a>
                    </div>
                </nav>
                <div class="main">
                    <slot></slot>
                </div>
            </div>
        `;
    }

    _toggleDrawer()
    {
        const drawer = this.shadowRoot.querySelector('nav');
        drawer.classList.toggle('open');
    }
}
customElements.define('helmholtz-marketplace-app', HelmholtzMarketplaceApp);
