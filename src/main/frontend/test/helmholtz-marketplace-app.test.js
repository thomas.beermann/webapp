import { html, fixture, expect } from '@open-wc/testing';

import '../src/helmholtz-marketplace-app.js';

describe('Helmholtz Marketplace App Shell', () => {
    let element;
    beforeEach(async () => {
        element = await fixture(html`
            <helmholtz-marketplace-app></helmholtz-marketplace-app>
        `);
    });
    it('renders a body', async () => {
        const body = element.shadowRoot.querySelector('.body');
        console.log(body);
        expect(body).to.exist;
        expect(body.classList.contains('body')).be.true;
    });
});
